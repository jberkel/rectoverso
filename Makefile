SCHEME := RectoVerso

test:
	xctool -scheme $(SCHEME) -sdk iphonesimulator test

clean:
	xctool -scheme $(SCHEME) -sdk iphonesimulator clean

update_carthage:
	carthage update --configuration Release --platform ios --no-use-binaries

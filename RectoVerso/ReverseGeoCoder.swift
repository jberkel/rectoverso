import Foundation

public protocol ReverseGeoCoder {
    func reverseCode(lat: Double, lon: Double, completion: (GeoCodingResult?) -> ())
}

public func defaultReverseGeoCoder(key: String) -> ReverseGeoCoder {
    return MapQuestReverseGeoCoder(key: key)
}

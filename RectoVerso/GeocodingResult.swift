import Foundation
/*
$ curl -q 'http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&lat=41.14891118694726&lon=-8.617819608502986' |jsonpretty
{
    "place_id": "2644010163",
    "licence": "Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright",
    "osm_type": "way",
    "osm_id": "233007631",
    "lat": "41.14879685",
    "lon": "-8.61797402532985",
    "display_name": "Home17, 17, Travessa do Carregal, Cordoaria, Centro, Cedofeita, St Ildefonso, Sé , Miragaia, S. Nicolau e Vitória, Porto, 4050-284, Portugal",
    "address": {
        "guest_house": "Home17",
        "house_number": "17",
        "road": "Travessa do Carregal",
        "neighbourhood": "Cordoaria",
        "suburb": "Centro",
        "city": "Cedofeita, St Ildefonso, Sé , Miragaia, S. Nicolau e Vitória",
        "county": "Porto",
        "postcode": "4050-284",
        "country": "Portugal",
        "country_code": "pt"
    }
}
*/
public struct GeoCodingResult : CustomStringConvertible {
    public struct Address : CustomStringConvertible {
        public let road: String?
        public let houseNumber: String?
        public let neighbourhood: String?
        public let postcode: String?
        public let city: String?
        public let suburb: String?
        public let county: String?
        public let stateDistrict: String?
        public let state: String?
        public let country: String?
        public let countryCode: String?
        public let attributes: [String:String]

        public var description: String {
            return "\(road) \(houseNumber) \(neighbourhood) \(suburb) \(city) \(country)"
        }
    }

    public let license: String
    public let placeId: String

    public let osmId: String
    public let osmType: String

    public let displayName: String
    public let lat: Double
    public let lon: Double
    public let address: Address?

    public var description: String {
        return displayName
    }
}

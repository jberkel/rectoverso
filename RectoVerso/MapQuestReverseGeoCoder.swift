import Foundation
import Argo

class AddressKeys {
    static let road = "road"
    static let houseNumber = "house_number"
    static let neighbourhood = "neighbourhood"
    static let postcode = "postcode"
    static let city = "city"
    static let suburb = "suburb"
    static let county = "county"
    static let state = "state"
    static let stateDistrict = "state_district"
    static let country = "country"
    static let countryCode = "country_code"
    static let regular = Set([road, houseNumber, neighbourhood, postcode, city, suburb, county, state, stateDistrict, country, countryCode])
}

extension GeoCodingResult.Address: Decodable {
    static func create(road: String?)(houseNumber: String?)(neighbourhood: String?)(postcode: String?)(city: String?)
        (suburb: String?)(county: String?)(stateDistrict: String?)(state: String?)(country: String?)(countryCode: String?)
        (attributes: [String:String]) -> GeoCodingResult.Address {

            return GeoCodingResult.Address(road: road, houseNumber: houseNumber, neighbourhood: neighbourhood,
                postcode: postcode, city: city, suburb: suburb, county: county,
                stateDistrict: stateDistrict, state: state , country: country,
                countryCode: countryCode, attributes: attributes)
    }

    public static func decode(j: JSON) -> Decoded<GeoCodingResult.Address> {
        return GeoCodingResult.Address.create
            <^> j <|? AddressKeys.road
            <*> j <|? AddressKeys.houseNumber
            <*> j <|? AddressKeys.neighbourhood
            <*> j <|? AddressKeys.postcode
            <*> j <|? AddressKeys.city
            <*> j <|? AddressKeys.suburb
            <*> j <|? AddressKeys.county
            <*> j <|? AddressKeys.stateDistrict
            <*> j <|? AddressKeys.state
            <*> j <|? AddressKeys.country
            <*> j <|? AddressKeys.countryCode
            <*> extractUnknownAttributes(j)
    }

    private static func extractUnknownAttributes(j: JSON) -> Decoded<[String:String]> {
        var attributes = [String:String]()
        switch j {
            case .Object(let dict):
                for (k,v) in dict {
                    switch v {
                        case .String(let s):
                            if !AddressKeys.regular.contains(k) {
                                attributes[k] = s
                        }
                        default: break
                    }
            }
            default: break
        }
        return Decoded.Success(attributes)
    }
}

extension GeoCodingResult: Decodable {
    static func create(license: String)(placeId: String)(osmId: String)(osmType: String)(displayName: String)(lat: String)(lon: String)(address: Address) -> GeoCodingResult {
        return GeoCodingResult(license: license, placeId: placeId, osmId: osmId, osmType: osmType, displayName: displayName,
            lat: (lat as NSString).doubleValue, lon: (lon as NSString).doubleValue, address: address)
    }

    public static func decode(j: JSON) -> Decoded<GeoCodingResult> {
        return GeoCodingResult.create
            <^> j <| "licence"
            <*> j <| "place_id"
            <*> j <| "osm_id"
            <*> j <| "osm_type"
            <*> j <| "display_name"
            <*> j <| "lat"
            <*> j <| "lon"
            <*> j <| "address"
    }
}

public class MapQuestReverseGeoCoder : ReverseGeoCoder {
    private let baseUrl = "https://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&"
    private let session: NSURLSession
    private let key: String

    class Delegate : NSObject, NSURLSessionTaskDelegate {
        func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
            NSLog("completed: \(task)")
        }
    }

    public init(key: String) {
        self.key = key
        self.session = NSURLSession(
            configuration: NSURLSessionConfiguration.defaultSessionConfiguration(),
            delegate: Delegate(),
            delegateQueue:NSOperationQueue.mainQueue()
        )
    }

    public func reverseCode(lat: Double, lon: Double, completion: (GeoCodingResult?) -> ())  {
        let request = NSURLRequest(URL: self.constructURL(lat, lon: lon))
        let task = self.session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
            if let data = data {
                switch self.parseJSON(data) {
                    case .Success(let value): completion(value)
                    default: completion(nil)
                    }
            } else {
                completion(nil)
            }
        })
        task.priority = NSURLSessionTaskPriorityLow
        task.resume()
    }

    // http://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&lat=41.14891118694726&lon=-8.617819608502986
    private func constructURL(lat: Double, lon: Double) -> NSURL {
        let url = baseUrl + "lat=\(lat)&lon=\(lon)&key=\(key)"
        return NSURL(string: url)!
    }

    private func parseJSON(data: NSData) -> Decoded<GeoCodingResult> {
        do {
            let json:AnyObject = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0))
            return GeoCodingResult.decode(JSON(json))
        } catch let error as NSError {
            NSLog("error deserializing JSON: \(error)")
            return .Failure(DecodeError.TypeMismatch(expected: "", actual: ""))
        }
    }
}

import Quick
import Nimble
import RectoVerso
import Nocilla

class MapQuestReverseGeocoderSpec : QuickSpec {
    override func spec() {
        let testBundle = NSBundle(forClass: MapQuestReverseGeocoderSpec.self)
        let porto = testBundle.URLForResource("fixtures/porto_carregal", withExtension: "json")!.path!
        let defaultTimeout:NSTimeInterval = 2

        var subject:MapQuestReverseGeoCoder?

        beforeEach {
            subject = MapQuestReverseGeoCoder(key: "testAppKey")

        }
        beforeSuite {
            LSNocilla.sharedInstance().start()
        }

        afterSuite {
            LSNocilla.sharedInstance().stop()
        }

        afterEach {
            LSNocilla.sharedInstance().clearStubs()
        }

        describe("reverse coding") {
            let lat = 41.14879685
            let lon = -8.61797402532985
            let url = "https://open.mapquestapi.com/nominatim/v1/reverse.php?format=json&lat=41.14879685&lon=-8.61797402532985&key=testAppKey"

            context("when the request is successful") {
                beforeEach {
                    stubRequest("GET", url).andReturn(200).withHeaders(["Content-Type": "application/json"]).withBody(NSData(contentsOfFile: porto)!)
                }

                it("it returns the parsed result in the completion handler") {
                    let expectation = self.expectationWithDescription("wait geocoding")

                    subject!.reverseCode(lat, lon: lon, completion: {
                        (coding: GeoCodingResult?) in
                        expect(coding).notTo(beNil())
                        let result: GeoCodingResult = coding!
                        expect(result.lat).to(equal(lat))
                        expect(result.lon).to(equal(lon))
                        expect(result.osmId).to(equal("233007631"))
                        expect(result.osmType).to(equal("way"))
                        expect(result.displayName).to(equal("Home17, 17, Travessa do Carregal, Cordoaria, Centro, Cedofeita, St Ildefonso, Sé , Miragaia, S. Nicolau e Vitória, Porto, Área Metropolitana do Porto, Norte, 4050-284, Portugal"))
                        expect(result.license).to(equal("Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright"))
                        expect(result.lat).to(equal(41.14879685))
                        expect(result.lon).to(equal(-8.61797402532985))

                        expect(result.address).notTo(beNil())
                        expect(result.address!.road!).to(equal("Travessa do Carregal"))
                        expect(result.address!.houseNumber!).to(equal("17"))
                        expect(result.address!.neighbourhood!).to(equal("Cordoaria"))
                        expect(result.address!.county!).to(equal("Porto"))
                        expect(result.address!.state!).to(equal("Norte"))
                        expect(result.address!.stateDistrict!).to(equal("Área Metropolitana do Porto"))
                        expect(result.address!.city!).to(equal("Cedofeita, St Ildefonso, Sé , Miragaia, S. Nicolau e Vitória"))
                        expect(result.address!.suburb!).to(equal("Centro"))
                        expect(result.address!.postcode!).to(equal("4050-284"))
                        expect(result.address!.country!).to(equal("Portugal"))
                        expect(result.address!.countryCode!).to(equal("pt"))
                        // extra attrs
                        expect(result.address!.attributes.count).to(equal(1))
                        expect(result.address!.attributes["guest_house"]!).to(equal("Home17"))
                        expectation.fulfill()
                    })
                    self.waitForExpectationsWithTimeout(defaultTimeout, handler: { (error) in XCTAssertNil(error, "timeout: \(error)") })
                }
            }

            context("when the request is NOT sucessful") {
                beforeEach {
                    stubRequest("GET", url).andReturn(404)
                }

                it("it returns the parsed result in the completion handler") {
                    let expectation = self.expectationWithDescription("wait geocoding")

                    subject!.reverseCode(lat, lon: lon, completion: {
                        (coding: GeoCodingResult?) in
                        expect(coding).to(beNil())
                        expectation.fulfill()
                    })
                    self.waitForExpectationsWithTimeout(defaultTimeout, handler: { (error) in XCTAssertNil(error, "timeout: \(error)") })
                }
            }
        }
    }
}

# Contributing

Fork, then clone the repo:

    $ git clone git@gitlab.com:username/rectoverso.git

Make your modifications, be sure to add tests where it makes sense.

Verify that all tests pass:

    $ make test

Submit a merge request on gitlab.com.
